xinput list-props "Wacom Intuos PT S 2 Finger"
xinput set-prop "Wacom Intuos PT S 2 Finger" "Synaptics Two-Finger Scrolling" 1, 1
xinput set-prop "Wacom Intuos PT S 2 Finger" "Device Accel Profile" 2
xinput set-prop "Wacom Intuos PT S 2 Finger" "Device Accel Constant Deceleration" 1.6
xinput set-prop "Wacom Intuos PT S 2 Finger" "Synaptics Scrolling Distance" 25 25
